import tkinter as tk
import customtkinter as ctk
from utils.menubar import MenuBar

WIN_TITLE = "Data-Cubes Generator"
WIN_GEOM = "1240x720"


class MainWindow(ctk.CTk):
    def __init__(self):
        super().__init__()
        self.geometry(WIN_GEOM)
        self.title(WIN_TITLE)

        self.menu = MenuBar(self)


if __name__ == '__main__':
    win = MainWindow()
    win.mainloop()
