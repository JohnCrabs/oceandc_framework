import tkinter as tk
from utils.menubar import MenuBar

WIN_TITLE = "Data-Cubes Generator"
WIN_GEOM = "1240x720"


class MainWindow:
    def __init__(self):
        self.root = tk.Tk()
        self.root.geometry(WIN_GEOM)
        self.root.title(WIN_TITLE)

        self.menu = MenuBar(self.root)

    def mainloop(self, n: int = 0):
        self.root.mainloop(n=n)


if __name__ == '__main__':
    win = MainWindow()
    win.mainloop()
