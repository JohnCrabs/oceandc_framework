import tkinter as tk


class MenuBar(tk.Menu):
    def __init__(self, parent):
        super().__init__(parent)
        self.filemenu = tk.Menu(self, tearoff=0)
        self.filemenu.add_command(label="New Project", command=self.NewProject)
        self.filemenu.add_command(label="Open Project", command=self.OpenProject)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", command=parent.quit)
        self.add_cascade(label="File", menu=self.filemenu)

        parent.config(menu=self)

    def NewProject(self):
        pass

    def OpenProject(self):
        pass


if __name__ == "__main__":
    pass
